; MonitorInputSwitcher.ahk
;
; Switches the monitor input from HDMI to DisplayPort and back with a quick double keypress of `ScrollLock`
; See: https://autohotkey.com/board/topic/96884-change-monitor-input-source (old thread, deprecated).
; See: https://www.reddit.com/r/AutoHotkey/comments/5xoqb6/monitor_input_switcher_ahk_forums/ (updated thread).


; General configuration.
#SingleInstance force
#NoEnv
#NoTrayIcon


; VCP codes for the different monitor input modes.
hdmiInput := 0x311
displayPortInput := 0x30F
currentInputSource := 0x30F


; A quick double keypress of `ScrollLock` switches between video inputs.
sc046::
if (A_PriorHotkey <> "sc046" or A_TimeSincePriorHotkey > 400) {
    KeyWait, RControl
    return
}
switchMonitorInputSource()
return


; Finds the monitor handle which will be used to send commands.
;
; @returns Monitor handle.
getMonitorHandle() {
    hMon := DllCall("MonitorFromPoint"
        , "int64", 0
        , "uint", 1)

    ; Gets Physical Monitor from handle.
    VarSetCapacity(Physical_Monitor, 8 + 256, 0)

    DllCall("dxva2\GetPhysicalMonitorsFromHMONITOR"
        , "int", hMon
        , "uint", 1
        , "int", &Physical_Monitor)

    return hPhysMon := NumGet(Physical_Monitor)
}


; Gets rid of the monitor handle once it's no longer needed.
;
; @param handle Monitor handle to get rid of.
destroyMonitorHandle(handle) {
    DllCall("dxva2\DestroyPhysicalMonitor"
        , "int", handle)
}


; Changes the monitor input source
;
; @param source Numeric code of the input source.
setMonitorInputSource(source) {
    handle := getMonitorHandle()

    DllCall("dxva2\SetVCPFeature"
        , "int", handle
        , "char", 0x60
        , "int", source)

    destroyMonitorHandle(handle)
}


; Gets the monitor input source.
;
; @returns Monitor input source.
getMonitorInputSource() {
    handle := getMonitorHandle()

    DllCall("dxva2\GetVCPFeatureAndVCPFeatureReply"
        , "int", handle
        , "char", 0x60 ;VCP code for Input Source Select
        , "Ptr", 0
        , "uint*", currentValue
        , "uint*", maximumValue)

    destroyMonitorHandle(handle)

    return currentValue
}


; Switches monitor input to HDMI.
switchMonitorInputToHdmi() {
    global hdmiInput

    setMonitorInputSource(hdmiInput)

    return
}


; Switches monitor input to DisplayPort.
switchMonitorInputToDisplayPort() {
    global displayPortInput

    setMonitorInputSource(displayPortInput)

    return
}


; Switches between HDMI and DisplayPort monitor input sources.
switchMonitorInputSource() {
    global hdmiInput
    global displayPortInput
    global currentInputSource

    switch currentInputSource {
        case hdmiInput:
            currentInputSource := displayPortInput
            switchMonitorInputToDisplayPort()
            return

        case displayPortInput:
            currentInputSource := hdmiInput
            switchMonitorInputToHdmi()
            return

        default:
            currentInputSource := displayPortInput
            switchMonitorInputToDisplayPort()
            return
    }
}